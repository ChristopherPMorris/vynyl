//
//  ViewController.swift
//  VYNYL
//
//  Created by Christopher Morris on 2/20/19.
//  Copyright © 2019 Christopher Morris. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var originalLabel: UILabel!
    @IBOutlet weak var flattenedLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        ////////////////////////////////////////////////////////////////////////////////
        // This view controller doesn't do much. The code is in the Array extension
        // and the code is tested in the VYNYLTests project.
        ////////////////////////////////////////////////////////////////////////////////
        let array: [Any] = [["one", "two", ["three"]], "four"]

        originalLabel.text = array.description

        let flatArray = array.flatten()

        flattenedLabel.text = flatArray.description
    }
}

