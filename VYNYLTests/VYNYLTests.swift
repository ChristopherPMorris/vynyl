//
//  VYNYLTests.swift
//  VYNYLTests
//
//  Created by Christopher Morris on 2/20/19.
//  Copyright © 2019 Christopher Morris. All rights reserved.
//

import XCTest
@testable import VYNYL

class VYNYLTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test1()
    {
        let array: [Any] = []
        
        let flatArray = array.flatten()
        
        XCTAssert(flatArray.isEmpty)
    }

    func test2()
    {
        let array: [Any] = [["one", ["two", "three"], ["four"]], "five"]
        
        let flatArray = array.flatten()
        
        XCTAssert(flatArray.elementsEqual(["one", "two", "three", "four", "five"]))
    }

    func test3()
    {
        let array: [Any] = [[[["one"], ["two", ["three"]], ["four"]], "five"], "six"]
        
        let flatArray = array.flatten()
        
        XCTAssert(flatArray.elementsEqual(["one", "two", "three", "four", "five", "six"]))
    }

    func test4()
    {
        let array: [Any] = [[[[1], [2, [3]], [4]], 5], 6]
        
        let flatArray = array.flatten()
        
        XCTAssert(flatArray.isEmpty)
    }

    func test3Performance()
    {
        // This is an example of a performance test case.
        self.measure {
            let array: [Any] = [[[["one"], ["two", ["three"]], ["four"]], "five"], "six"]
            
            let flatArray = array.flatten()
            
            XCTAssert(flatArray.elementsEqual(["one", "two", "three", "four", "five", "six"]))
        }
    }

}
