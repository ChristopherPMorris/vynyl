//
//  Array+Extenstions.swift
//  VYNYL
//
//  Created by Christopher Morris on 2/20/19.
//  Copyright © 2019 Christopher Morris. All rights reserved.
//

import Foundation

extension Array
{
    ////////////////////////////////////////////////////////////////////////////////
    // This will only flatten an array of strings. An array with any other type
    // elements will return an empty array. This is demonstrated in VYNYLTetsts,
    // test4.
    ////////////////////////////////////////////////////////////////////////////////
    func flatten() -> [String]
    {
        var flatArray = [String]()
        
        for element in self
        {
            if let item = element as? [Any]
            {
                let subArray = item.flatten()
                flatArray.append(contentsOf: subArray)
            }
            else if let item = element as? String
            {
                flatArray.append(item)
            }
        }
        
        return flatArray
    }
}
